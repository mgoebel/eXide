# Ubuntu (git, ant, curl)
#
# VERSION               0.0.1

FROM ubuntu:bionic

LABEL Description="image for a eXide build ecosystem" Version="1.0"

RUN apt-get update && apt-get install -y git ant curl patch